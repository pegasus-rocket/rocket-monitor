import { AfterViewInit, Component, Input } from '@angular/core';
import { Rocket } from '../../../models/rocket.model';
import * as echarts from 'echarts';
import { EChartsOption, EChartsType } from 'echarts';

@Component({
  selector: 'app-acceleration',
  templateUrl: './acceleration.component.html',
  styleUrl: './acceleration.component.scss',
  standalone: false
})
export class AccelerationComponent implements AfterViewInit {
  @Input({ required: true }) rocket: Rocket;
  @Input({ required: true }) axis: 'x' | 'y' | 'z' | 'all';
  @Input() height: string = '450';
  dataX: [Date, number][] = [];
  dataY: [Date, number][] = [];
  dataZ: [Date, number][] = [];
  chart: EChartsType;

  options: EChartsOption = {
    visualMap: {
      show: false,
      type: 'continuous',
      seriesIndex: 0
    },
    xAxis: {
      type: 'time',
      name: 'Time',
      splitLine: {
        show: false
      },
      show: false
    },
    yAxis: {
      name: 'm / s ^ 2',
      type: 'value',
      boundaryGap: [0, '100%'],
      splitLine: {
        show: false
      }
    },
    legend: {
      top: '5%',
      left: 'center',
      icon: 'rect',
      textStyle: {
        color: 'rgba(255,252,252,0.78)'
      }
    },
    series: [
      {
        name: 'X',
        type: 'line',
        showSymbol: false,
        data: this.dataX,
        smooth: true,
        itemStyle: {
          color: '#FF7028'
        },
        lineStyle: {
          color: '#FF7028',
          shadowBlur: 30,
          shadowColor: '#FF7028',
          shadowOffsetY: 10
        }
      },
      {
        name: 'Y',
        type: 'line',
        showSymbol: false,
        data: this.dataX,
        smooth: true,
        itemStyle: {
          color: '#49D5FD'
        },
        lineStyle: {
          color: '#49D5FD',
          shadowBlur: 30,
          shadowColor: '#49D5FD',
          shadowOffsetY: 10
        }
      },
      {
        name: 'Z',
        type: 'line',
        showSymbol: false,
        data: this.dataX,
        smooth: true,
        itemStyle: {
          color: '#76DF11'
        },
        lineStyle: {
          color: '#76DF11',
          shadowBlur: 30,
          shadowColor: '#76DF11',
          shadowOffsetY: 10
        }
      }
    ]
  };

  ngAfterViewInit() {
    this.chart = echarts.init(
      document.getElementById('acceleration-' + this.axis),
      null,
      {
        width: document.getElementById('acceleration-' + this.axis).offsetWidth
      }
    );
    this.chart.setOption(this.options);

    setInterval(() => {
      if (!this.rocket.accelerations) {
        this.resetChart();
        return;
      }
      switch (this.axis) {
        case 'x':
          // this.dataX.push(this.randomData());
          this.dataX = this.rocket.accelerations.map(x => [
            x[0],
            x[1][this.axis]
          ]);
          break;
        case 'y':
          // this.dataY.push(this.randomData());
          this.dataY = this.rocket.accelerations.map(x => [
            x[0],
            x[1][this.axis]
          ]);
          break;
        case 'z':
          this.dataZ.push(this.randomData());
          this.dataZ = this.rocket.accelerations.map(x => [
            x[0],
            x[1][this.axis]
          ]);
          break;
        case 'all':
          // this.dataX.push(this.randomData());
          // this.dataY.push(this.randomData());
          // this.dataZ.push(this.randomData());
          this.dataX = this.rocket.accelerations.map(x => [x[0], x[1].x]);
          this.dataY = this.rocket.accelerations.map(x => [x[0], x[1].y]);
          this.dataZ = this.rocket.accelerations.map(x => [x[0], x[1].z]);
          break;
      }
      this.updateChart();
    }, 100);
  }

  updateChart() {
    this.chart.setOption({
      series: [
        {
          data: this.dataX
        },
        {
          data: this.dataY
        },
        {
          data: this.dataZ
        }
      ]
    });
  }

  resetChart() {
    this.dataX = [[new Date(), 0]];
    this.dataY = [[new Date(), 0]];
    this.dataZ = [[new Date(), 0]];
    this.updateChart();
  }

  randomData(): [Date, number] {
    const maxLength = 15;
    if (this.dataX.length > maxLength) {
      this.dataX.splice(0, this.dataX.length - maxLength);
    }
    if (this.dataY.length > maxLength) {
      this.dataY.splice(0, this.dataY.length - maxLength);
    }
    if (this.dataZ.length > maxLength) {
      this.dataZ.splice(0, this.dataZ.length - maxLength);
    }
    const value = Math.round(Math.abs((Math.random() * 10000) % 1000));
    return [new Date(), value];
  }

  protected readonly String = String;
}

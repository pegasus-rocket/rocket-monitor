import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
  standalone: false
})
export class LogsComponent implements OnChanges {
  @Input() data: string;
  logs: string[] = [];
  protected readonly JSON = JSON;

  ngOnChanges() {
    if (this.data) {
      this.logs.push(this.data);
      if (this.logs.length > 3) {
        this.logs.splice(0, this.logs.length - 3);
      }
    }
  }
}

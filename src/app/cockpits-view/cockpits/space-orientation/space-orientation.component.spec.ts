import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceOrientationComponent } from './space-orientation.component';

describe('SpaceOrientationComponent', () => {
  let component: SpaceOrientationComponent;
  let fixture: ComponentFixture<SpaceOrientationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpaceOrientationComponent]
    });
    fixture = TestBed.createComponent(SpaceOrientationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

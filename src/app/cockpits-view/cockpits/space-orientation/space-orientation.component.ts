import { Component } from '@angular/core';

@Component({
  selector: 'app-space-orientation',
  templateUrl: './space-orientation.component.html',
  styleUrls: ['./space-orientation.component.scss'],
  standalone: false
})
export class SpaceOrientationComponent {}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IgnitionDialogComponent } from './ignition-dialog.component';

describe('IgnitionDialogComponent', () => {
  let component: IgnitionDialogComponent;
  let fixture: ComponentFixture<IgnitionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IgnitionDialogComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(IgnitionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

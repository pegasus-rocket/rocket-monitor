import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatInput } from '@angular/material/input';
import { invoke } from '@tauri-apps/api/core';
import { FormsModule } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-ignition-dialog',
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    MatInput
  ],
  templateUrl: './ignition-dialog.component.html',
  styleUrl: './ignition-dialog.component.scss'
})
export class IgnitionDialogComponent {
  async onMotorIgnition() {
    console.log('Fire button pressed !');
    await invoke<void>('release_rocket');
  }
}

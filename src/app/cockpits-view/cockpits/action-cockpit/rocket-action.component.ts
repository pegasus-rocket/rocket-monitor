import { ParachuteDialogComponent } from './parachute-dialog/parachute-dialog.component';
import { IgnitionDialogComponent } from './ignition-dialog/ignition-dialog.component';
import { SnackBarService } from '../../../services/snackbar.service';
import { DeviceService } from '../../../services/device.service';
import { Component, inject, input } from '@angular/core';
import { Rocket } from '../../../models/rocket.model';
import { MatDialog } from '@angular/material/dialog';
import { invoke } from '@tauri-apps/api/core';

@Component({
  selector: 'app-actions-cockpit',
  templateUrl: './rocket-action.component.html',
  styleUrls: ['./rocket-action.component.scss'],
  standalone: false
})
export class RocketActionComponent {
  rocket = input.required<Rocket>();
  readonly dialog = inject(MatDialog);

  constructor(
    private snackService: SnackBarService,
    private deviceService: DeviceService
  ) {}

  async onReleaseParachute() {
    if (this.rocket().status.system === 'unknown')
      return this.snackService.showMessage(
        'Warning ! Radio communication error !'
      );
    if (this.deviceService.getDevice('parachute').status == 'fired')
      return this.snackService.showMessage('Parachute already fired');
    else if (this.rocket().status.flyState === 'on_ground')
      return this.dialog.open(ParachuteDialogComponent, {
        width: '400px'
      });
    await invoke<void>('release_parachute');
  }

  onReleaseDialog() {
    if (this.rocket().status.system === 'unknown')
      return this.snackService.showMessage(
        'Warning ! Radio communication error !'
      );
    if (this.rocket().serial.isDebugMode)
      return this.snackService.showMessage(
        "Motor can't be ignited is debugging mode."
      );
    if (this.rocket().status.system !== 'ready')
      return this.snackService.showMessage('Rocket is not ready to take off.');
    if (this.rocket().status.isBaseLocked)
      return this.snackService.showMessage(
        'The base is locked. Impossible to ignite the motor.'
      );
    this.dialog.open(IgnitionDialogComponent, {
      width: '400px'
    });
  }

  async onClearStorage() {
    await invoke<void>('clear_rocket_storage');
  }
}

import { SnackBarService } from '../../../../services/snackbar.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButton } from '@angular/material/button';
import { MatInput } from '@angular/material/input';
import { invoke } from '@tauri-apps/api/core';
import { Component } from '@angular/core';

@Component({
  selector: 'app-parachute-dialog',
  imports: [MatButton, MatInput, MatDialogModule, MatFormFieldModule],
  templateUrl: './parachute-dialog.component.html',
  styleUrl: './parachute-dialog.component.scss'
})
export class ParachuteDialogComponent {
  constructor(private snackService: SnackBarService) {}

  async onReleaseParachute() {
    console.log('Fire button pressed !');
    await invoke<void>('release_parachute');
    this.snackService.showMessage('Parachute deployment order sent !');
  }
}

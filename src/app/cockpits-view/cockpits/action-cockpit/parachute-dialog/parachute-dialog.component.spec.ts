import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParachuteDialogComponent } from './parachute-dialog.component';

describe('ParachuteDialogComponent', () => {
  let component: ParachuteDialogComponent;
  let fixture: ComponentFixture<ParachuteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ParachuteDialogComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ParachuteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

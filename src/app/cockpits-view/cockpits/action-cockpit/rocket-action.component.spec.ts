import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RocketActionComponent } from './rocket-action.component';

describe('ParachuteCockpitComponent', () => {
  let component: RocketActionComponent;
  let fixture: ComponentFixture<RocketActionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RocketActionComponent]
    });
    fixture = TestBed.createComponent(RocketActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

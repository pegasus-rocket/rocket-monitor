import { FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-device-status-settings',
  templateUrl: './device-status-settings.component.html',
  styleUrls: ['./device-status-settings.component.scss'],
  standalone: false
})
export class DeviceStatusSettingsComponent {
  constructor(private _formBuilder: FormBuilder) {}
}

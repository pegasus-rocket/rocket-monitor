import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceStatusSettingsComponent } from './device-status-settings.component';

describe('DeviceStatusSettingsComponent', () => {
  let component: DeviceStatusSettingsComponent;
  let fixture: ComponentFixture<DeviceStatusSettingsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceStatusSettingsComponent]
    });
    fixture = TestBed.createComponent(DeviceStatusSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

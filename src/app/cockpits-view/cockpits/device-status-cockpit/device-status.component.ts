import { Component, effect, input, OnChanges } from '@angular/core';
import { DeviceService } from '../../../services/device.service';
import { Rocket } from '../../../models/rocket.model';
import { Device } from '../../../models/device.model';

@Component({
  selector: 'app-device-status-cockpit',
  templateUrl: './device-status.component.html',
  styleUrls: ['./device-status.component.scss'],
  standalone: false
})
export class DeviceStatusComponent implements OnChanges {
  rocket = input.required<Rocket>();
  devices: Device[] = [];
  message: string;
  isReady: boolean;

  constructor(private deviceService: DeviceService) {
    effect(() => {
      this.devices = this.deviceService.devices();
      this.updateStatus();
    });
  }

  ngOnChanges() {
    this.updateStatus();
  }

  updateStatus() {
    if (this.rocket().status.system == 'unknown') {
      this.message = 'Can receive data from the rocket.';
      this.isReady = false;
    } else if (this.rocket().status.emergency) {
      this.message = 'Emergency has been enabled!';
      this.isReady = false;
    } else if (this.rocket().status.isBaseLocked) {
      this.message = 'The base is in security mode. Actions are disabled.';
      this.isReady = false;
    } else if (this.rocket().status.system == 'down') {
      this.message = 'System has been down!';
      this.isReady = false;
    } else {
      this.isReady = true;
      this.message = 'The rocket is ready.';
    }
  }
}

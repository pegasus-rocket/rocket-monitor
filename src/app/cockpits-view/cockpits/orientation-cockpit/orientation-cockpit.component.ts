import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-orientation-cockpit',
  templateUrl: './orientation-cockpit.component.html',
  styleUrls: ['./orientation-cockpit.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: false
})
export class OrientationCockpitComponent {
  @Input({ required: true }) orientation: number;
  degrees = Array.from(Array(360 / 5).keys()).map(x => x * 5);
  // degrees = Array.from(Array(10).keys()).map(x => x * 5);

  ngAfterViewInit() {
    const scroll = document.querySelector('.loop');

    const table1 = scroll.querySelector('table');

    // Create a copy of the table and adds it to the scrollable element
    const table2 = table1.cloneNode(true);
    scroll.appendChild(table2);

    const options = {
      root: scroll,
      rootMargin: '0px',
      threshold: 0
    };

    const callback = entries => {
      console.log(entries);
      if (!entries[0].isIntersecting) {
        // table1 is out of bounds, we can scroll back to it
        scroll.scrollLeft = 0;
      }
    };

    const observer = new IntersectionObserver(callback, options);
    observer.observe(table1);
  }
  getCoordinate(): string {
    if (this.orientation > 336 && this.orientation <= 22) {
      return 'N';
    } else if (this.orientation > 22 && this.orientation <= 68) {
      return 'NE';
    } else if (this.orientation > 68 && this.orientation <= 112) {
      return 'E';
    } else if (this.orientation > 112 && this.orientation <= 157) {
      return 'SE';
    } else if (this.orientation > 157 && this.orientation <= 202) {
      return 'S';
    } else if (this.orientation > 202 && this.orientation <= 247) {
      return 'SW';
    } else if (this.orientation > 247 && this.orientation <= 292) {
      return 'W';
    } else {
      return 'NW';
    }
  }
}

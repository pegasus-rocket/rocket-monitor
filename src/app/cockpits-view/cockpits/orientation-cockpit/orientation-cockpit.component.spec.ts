import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrientationCockpitComponent } from './orientation-cockpit.component';

describe('OrientationCockpitComponent', () => {
  let component: OrientationCockpitComponent;
  let fixture: ComponentFixture<OrientationCockpitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrientationCockpitComponent]
    });
    fixture = TestBed.createComponent(OrientationCockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

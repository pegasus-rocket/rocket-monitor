import { AfterViewInit, Component, Input } from '@angular/core';
import { Rocket } from '../../../models/rocket.model';
import * as echarts from 'echarts';
import { EChartsOption, EChartsType } from 'echarts';

@Component({
  selector: 'app-altitude-chart',
  templateUrl: './altitude-chart.component.html',
  styleUrls: ['./altitude-chart.component.scss'],
  standalone: false
})
export class AltitudeChartComponent implements AfterViewInit {
  @Input({ required: true }) rocket: Rocket;
  data: [Date, number][] = [];
  chart: EChartsType;
  date = new Date();

  options: EChartsOption = {
    visualMap: {
      show: false,
      type: 'continuous',
      seriesIndex: 0
    },
    xAxis: {
      type: 'time',
      name: 'Time',
      splitLine: {
        show: false
      }
    },
    yAxis: {
      min: 0,
      name: 'Altitude (m)',
      type: 'value',
      boundaryGap: [0, '100%'],
      splitLine: {
        show: false
      }
    },
    series: [
      {
        name: 'Fake Data',
        type: 'line',
        showSymbol: false,
        data: this.data,
        smooth: true,
        markPoint: {
          data: [{ type: 'max', name: 'Max' }]
        },
        itemStyle: {
          color: '#622DFA'
        },
        lineStyle: {
          color: '#49D5FD',
          shadowBlur: 30,
          shadowColor: '#49D5FD',
          shadowOffsetY: 10
        }
      }
    ]
  };

  ngAfterViewInit() {
    this.chart = echarts.init(document.getElementById('chart_altitude'), null, {
      width: document.getElementById('chart_altitude').offsetWidth + 100
    });
    this.chart.setOption(this.options);

    setInterval(() => {
      // if (!this.rocket.status.isIgnited || !this.rocket.altitudes) {
      //   this.resetChart();
      //   return;
      // }
      // this.data.push(this.randomData());
      this.data = this.rocket.altitudes;
      this.updateChart();
    }, 1000);
  }

  updateChart() {
    this.chart.setOption({
      series: [
        {
          data: this.data
        }
      ]
    });
  }

  resetChart() {
    this.data = [[new Date(), 0]];
    this.updateChart();
  }

  randomData(): [Date, number] {
    if (this.data.length > 60) {
      this.data.splice(0, this.data.length - 60);
    }
    const value = Math.round(Math.abs((Math.random() * 10000) % 1000));
    return [new Date(), value];
  }
}

import { SnackBarService } from '../../../services/snackbar.service';
import { writeText } from '@tauri-apps/plugin-clipboard-manager';
import { Component, Input, OnInit } from '@angular/core';
import { GeoJSONSource, Map } from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';
import { Rocket } from '../../../models/rocket.model';

@Component({
  selector: 'app-map-cockpit',
  templateUrl: './map-cockpit.component.html',
  styleUrl: './map-cockpit.component.scss',
  standalone: false
})
export class MapCockpitComponent implements OnInit {
  map: Map;
  @Input({ required: true }) rocket: Rocket;
  rocketCoords: [number, number] = [2.659946850494884, 46.95795312119979];
  liveTracking = false;
  zoom = 12;

  constructor(private snackService: SnackBarService) {}

  ngOnInit() {
    this.map = new Map({
      container: 'map',
      style:
        'https://api.maptiler.com/maps/hybrid/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL',
      center: this.rocketCoords, // starting position [lng, lat]
      zoom: 3
    });

    const updateRocketCoords = (): any => {
      console.log(this.rocket.coord);
      return {
        type: 'FeatureCollection',
        features: [
          {
            properties: null,
            type: 'Feature',
            geometry: {
              type: 'Point',
              coordinates: [
                (this.rocketCoords[0] = this.rocket.coord.longitude),
                (this.rocketCoords[1] = this.rocket.coord.latitude)
              ]
            }
          }
        ]
      };
    };

    this.map.on('load', async () => {
      // Load image only once
      const image = await this.map.loadImage('./assets/icons/rocket_map.png');
      this.map.addImage('cat', image.data);

      // Add the source and layer only once
      this.map.addSource('point', {
        type: 'geojson',
        data: updateRocketCoords()
      });

      this.map.addLayer({
        id: 'point',
        type: 'symbol',
        source: 'point',
        layout: {
          'icon-image': 'cat',
          'icon-size': 0.8
        }
      });

      const animateMarker = () => {
        const data = updateRocketCoords();

        // Update the point source data without excessive recalculations
        (this.map.getSource('point') as GeoJSONSource).setData(data);
        if (this.liveTracking && !this.map.isMoving()) {
          this.flyToRocket(100);
        }

        requestAnimationFrame(animateMarker);
      };

      // Start the animation.
      requestAnimationFrame(animateMarker);
    });

    this.map.on('mousedown', () => {
      this.liveTracking = false;
    });
  }

  onRelocateClicked() {
    this.flyToRocket(1.6);
    setTimeout(() => (this.liveTracking = true), 1000);
  }

  flyToRocket(speed: number = 1.2) {
    this.map.flyTo({
      center: this.rocketCoords,
      zoom: this.map.getZoom() > this.zoom ? this.map.getZoom() : this.zoom,
      essential: true,
      speed
    });
  }

  async onCopyCoords() {
    const coords =
      String(this.rocketCoords[0]) + ',' + String(this.rocketCoords[1]);
    await writeText(coords).then(() => {
      this.snackService.showMessage(coords + ', copied !');
    });
  }
}

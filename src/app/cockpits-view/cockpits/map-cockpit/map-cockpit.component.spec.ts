import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapCockpitComponent } from './map-cockpit.component';

describe('MapCockpitComponent', () => {
  let component: MapCockpitComponent;
  let fixture: ComponentFixture<MapCockpitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MapCockpitComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(MapCockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeDOrientationCockpitsComponent } from './three-d-orientation-cockpits.component';

describe('ThreeDOrientationCockpitsComponent', () => {
  let component: ThreeDOrientationCockpitsComponent;
  let fixture: ComponentFixture<ThreeDOrientationCockpitsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ThreeDOrientationCockpitsComponent]
    });
    fixture = TestBed.createComponent(ThreeDOrientationCockpitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

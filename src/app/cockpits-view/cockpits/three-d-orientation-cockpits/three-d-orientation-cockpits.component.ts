import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Orientation } from '../../../models/rocket.model';
import * as THREE from 'three';

@Component({
  selector: 'app-three-d-orientation-cockpits',
  templateUrl: './three-d-orientation-cockpits.component.html',
  styleUrls: ['./three-d-orientation-cockpits.component.scss'],
  standalone: false
})
export class ThreeDOrientationCockpitsComponent implements OnInit, OnChanges {
  @Input({ required: true }) orientation: Orientation;
  threeRenderer: THREE.WebGLRenderer;
  private rocketObject: GLTF;

  ngOnInit() {
    this.createThreeJsBox();
  }

  createThreeJsBox(): void {
    const canvas = document.getElementById('canvas-box');

    if (!canvas) {
      return;
    }

    const scene = new THREE.Scene();

    const gridHelper = new THREE.GridHelper(60);
    scene.add(gridHelper);

    const ambientLight = new THREE.AmbientLight(0xffffff, 1);
    scene.add(ambientLight);

    const color = 0xffffff;
    const intensity = 5;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(2, 1, 0);
    light.target.position.set(0, 0, 0);
    scene.add(light);
    scene.add(light.target);

    const pointLight = new THREE.PointLight(0xffffff, 1);
    pointLight.position.x = 0;
    pointLight.position.y = 0;
    pointLight.position.z = 0;
    scene.add(pointLight);

    const canvasSizes = {
      width: canvas.offsetWidth,
      height: canvas.offsetHeight
    };

    const camera = new THREE.PerspectiveCamera(
      50,
      canvasSizes.width / canvasSizes.height,
      0.001,
      1000
    );
    let direction = new THREE.Vector3(0, 12, 30);
    camera.position.add(direction);

    scene.add(camera);

    this.threeRenderer = new THREE.WebGLRenderer({
      canvas: canvas,
      antialias: true
    });
    this.threeRenderer.setClearColor(0xe0c1420, 1);
    this.threeRenderer.setSize(canvasSizes.width, canvasSizes.height);

    // window.addEventListener('resize', () => {
    //   canvasSizes.width = window.innerWidth;
    //   canvasSizes.height = window.innerHeight;
    //
    //   camera.aspect = canvasSizes.width / canvasSizes.height;
    //   camera.updateProjectionMatrix();
    //
    //   this.threeRenderer.setSize(
    //     canvasSizes.width * window.devicePixelRatio,
    //     canvasSizes.height * window.devicePixelRatio
    //   );
    //   this.threeRenderer.setPixelRatio(window.devicePixelRatio);
    //   this.threeRenderer.render(scene, camera);
    // });

    // "V2 Rocket" (https://skfb.ly/6YZCS) by Diccbudd is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
    new GLTFLoader().load(
      './assets/3D/v2_rocket.glb',
      (gltf: GLTF) => {
        this.rocketObject = gltf;
        this.rocketObject.scene.scale.set(1, 1, 1);
        this.rocketObject.scene.position.y = 11.9;
        scene.add(this.rocketObject.scene);
      },
      undefined,
      function (error) {
        console.error(error);
      }
    );

    const animate = () => {
      requestAnimationFrame(animate);
      this.threeRenderer.render(scene, camera);
    };

    animate();
  }

  ngOnChanges() {
    if (!this.rocketObject.scene) {
      return;
    }

    const rotationAngle = Math.PI / 2;

    // Create a quaternion representing the desired rotation
    const rotationQuaternion = new THREE.Quaternion();
    rotationQuaternion.setFromAxisAngle(
      new THREE.Vector3(1, 0, 0),
      rotationAngle
    );

    // Other possibilities
    // x,z,y,w
    // z y w x
    // Multiply the rotation quaternion with the current orientation quaternion
    const targetQuaternion = new THREE.Quaternion(
      this.orientation.z,
      this.orientation.x,
      this.orientation.w,
      this.orientation.y
    );
    targetQuaternion.premultiply(rotationQuaternion);

    this.rocketObject.scene.quaternion.slerp(targetQuaternion, 1);
  }
}

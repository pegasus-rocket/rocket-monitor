import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RocketService } from '../../../services/rocket.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { invoke } from '@tauri-apps/api/core';

interface Port {
  port_name: string;
}

@Component({
  selector: 'app-serial-config-cockpit',
  templateUrl: './serial-config-cockpit.component.html',
  styleUrls: ['./serial-config-cockpit.component.scss'],
  standalone: false
})
export class SerialConfigCockpitComponent implements OnInit, OnDestroy {
  ports: Port[] = [];
  bauds: number[] = [750, 1200, 9600, 74880, 250000, 500000, 2000000];
  serialForm: FormGroup = new FormGroup({
    port: new FormControl<Port>(null, Validators.required),
    baud: new FormControl<number>(9600, Validators.required)
  });
  currentTime: string;
  intervalId: number;

  constructor(private rocketService: RocketService) {}

  ngOnInit() {
    setInterval(() => {
      invoke<Port[]>('get_serial_ports').then((ports: Port[]) => {
        this.ports = ports;
        if (this.ports.length <= 0) {
          this.serialForm.get('port')?.disable();
        } else {
          this.serialForm.get('port')?.enable();
        }
      });
    }, 1000);
    this.intervalId = setInterval(() => this.getDate(), 1000);
  }

  getDate() {
    const hours =
      new Date().getHours() < 10
        ? '0' + new Date().getHours().toString()
        : new Date().getHours().toString();
    const minutes =
      new Date().getMinutes() < 10
        ? '0' + new Date().getMinutes().toString()
        : new Date().getMinutes().toString();
    const seconds =
      new Date().getSeconds() < 10
        ? '0' + new Date().getSeconds().toString()
        : new Date().getSeconds().toString();
    this.currentTime = `${hours}:${minutes}:${seconds}`;
  }

  async updateSettings() {
    if (this.serialForm.valid) {
      const port: Port = this.serialForm.get('port').value;
      const baud: number = this.serialForm.get('baud').value;
      invoke<void>('update_serial_settings', { port, baud }).then(() => {
        this.rocketService.resetRocket();
        invoke('read_serial');
      });
    }
  }

  async onSelectPortOption(port: Port) {
    this.serialForm.get('port').setValue(port.port_name);
    await this.updateSettings();
  }

  async onSelectBaudOption(baud: number) {
    this.serialForm.get('baud').setValue(baud);
    await this.updateSettings();
  }

  ngOnDestroy() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }
}

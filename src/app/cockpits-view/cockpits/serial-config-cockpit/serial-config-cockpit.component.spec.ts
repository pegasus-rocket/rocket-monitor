import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SerialConfigCockpitComponent } from './serial-config-cockpit.component';

describe('SerialConfigCockpitComponent', () => {
  let component: SerialConfigCockpitComponent;
  let fixture: ComponentFixture<SerialConfigCockpitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SerialConfigCockpitComponent]
    });
    fixture = TestBed.createComponent(SerialConfigCockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
  selector: 'app-simulations',
  templateUrl: './simulations.component.html',
  styleUrls: ['./simulations.component.scss'],
  standalone: false
})
export class SimulationsComponent {
  simulations: { name: string }[] = [{ name: 'Pegasus' }, { name: 'Electron' }];
  rocketType: string[];
  simulationForm: FormGroup = new FormGroup({
    rocketType: new FormControl('minif', Validators.required),
    rocketDiameter: new FormControl(null, [
      Validators.required,
      Validators.min(0)
    ]),
    CGCPSpace: new FormControl(null, [Validators.required, Validators.min(0)])
  });

  onchangeRocketType(type: string) {
    this.simulationForm.get('rocketType').setValue(type);
  }
}

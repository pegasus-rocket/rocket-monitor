import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-speed-cockpit',
  templateUrl: './speed-cockpit.component.html',
  styleUrls: ['./speed-cockpit.component.scss'],
  standalone: false
})
export class SpeedCockpitComponent {
  @Input({ required: true }) speed: number; // Rocket speed in km/h
  progressConfig = {
    stroke: 25,
    radius: 150,
    duration: 800,
    rounded: false,
    clockwise: true,
    semicircle: true,
    color: '#5928E7',
    responsive: true,
    background: '#1E253E',
    animation: 'easeInOutQuart'
  };

  getMax(): number {
    return Math.ceil(this.speed / 100) * 100;
  }

  getOverlayStyle() {
    const isSemi = this.progressConfig.semicircle;
    const transform = 'translateX(-50%)';

    return {
      top: isSemi ? 'auto' : '50%',
      bottom: isSemi ? '5%' : 'auto',
      left: '50%',
      transform,
      fontSize: this.progressConfig.radius / 3.5 + 'px'
    };
  }
}

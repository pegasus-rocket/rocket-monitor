import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeedCockpitComponent } from './speed-cockpit.component';

describe('SpeedCockpitComponent', () => {
  let component: SpeedCockpitComponent;
  let fixture: ComponentFixture<SpeedCockpitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpeedCockpitComponent]
    });
    fixture = TestBed.createComponent(SpeedCockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

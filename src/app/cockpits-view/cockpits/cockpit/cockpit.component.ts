import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss'],
  standalone: false
})
export class CockpitComponent {
  @Input({ required: true }) title: string;
  @Input() noPadding: boolean = false;
  @Input() topLine: boolean = true;
  @Input() settingsBtn: boolean = false;
  @Input() locateBtn: boolean = false;
  @Input() resetBtn: boolean = false;
  @Input() copyBtn: boolean = false;
  @Output() settingsClicked = new EventEmitter<boolean>();
  @Output() copyBtnClicked = new EventEmitter<boolean>();
  @Output() locateClicked = new EventEmitter<boolean>();
  @Output() resetBtnClicked = new EventEmitter<boolean>();
  @Input() tiny: boolean = false;
  randomId = Math.random().toString();

  onOpenSettings() {
    this.settingsClicked.emit(true);
  }

  onReset() {
    this.resetBtnClicked.emit(true);
  }

  onLocate() {
    this.locateClicked.emit(true);
  }

  onCopy() {
    this.copyBtnClicked.emit(true);
  }

  isCollapsed() {
    return (
      document
        .getElementById('button-' + this.randomId)
        .getAttribute('aria-expanded') === 'true'
    );
  }
}

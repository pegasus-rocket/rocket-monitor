import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AltimeterCockpitComponent } from './altimeter-cockpit.component';

describe('AltimeterCockpitComponent', () => {
  let component: AltimeterCockpitComponent;
  let fixture: ComponentFixture<AltimeterCockpitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AltimeterCockpitComponent]
    });
    fixture = TestBed.createComponent(AltimeterCockpitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

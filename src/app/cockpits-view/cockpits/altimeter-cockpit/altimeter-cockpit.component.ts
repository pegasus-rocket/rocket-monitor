import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-altimeter-cockpit',
  templateUrl: './altimeter-cockpit.component.html',
  styleUrls: ['./altimeter-cockpit.component.scss'],
  standalone: false
})
export class AltimeterCockpitComponent {
  @Input({ required: true }) altitude: number;
  tare: number = 0;

  onTareAltitude() {
    this.tare = this.altitude;
  }

  getAltitudes(): number[] {
    let array = Array.from(
      { length: 7 },
      (_, i) => this.altitude - this.tare - 3 + i
    );
    return array.filter(x => x >= 0);
  }
}

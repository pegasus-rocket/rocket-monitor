import { Component, OnDestroy, OnInit } from '@angular/core';
import { RocketService } from '../services/rocket.service';
import { exit } from '@tauri-apps/plugin-process';
import { Rocket } from '../models/rocket.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fly-view',
  templateUrl: './cockpits.component.html',
  styleUrls: ['./cockpits.component.scss'],
  standalone: false
})
export class CockpitsComponent implements OnInit, OnDestroy {
  rocket: Rocket;
  rocketSubscription: Subscription;

  constructor(private rocketService: RocketService) {}
  ngOnInit() {
    this.rocketSubscription = this.rocketService.rocket.subscribe(rocket => {
      this.rocket = rocket;
    });
  }

  async onExitApp() {
    await exit(0);
  }

  ngOnDestroy() {
    this.rocketSubscription.unsubscribe();
  }
}

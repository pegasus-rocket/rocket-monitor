import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CockpitsComponent } from './cockpits.component';

describe('FlyViewComponent', () => {
  let component: CockpitsComponent;
  let fixture: ComponentFixture<CockpitsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CockpitsComponent]
    });
    fixture = TestBed.createComponent(CockpitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

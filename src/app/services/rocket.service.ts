import {
  Acceleration,
  Coordinates,
  Orientation,
  Rocket,
  RocketStatus
} from '../models/rocket.model';
import { DeviceId } from '../models/device.model';
import { DeviceService } from './device.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RocketService {
  rocket = new BehaviorSubject<Rocket>(null);
  isBaseConnected: boolean = false;

  constructor(private deviceService: DeviceService) {
    setInterval(() => {
      if (
        this.rocket === null ||
        (new Date().valueOf() - this.rocket.value.time.valueOf()) / 1000 > 0.7
      ) {
        this.resetRocket();
        this.isBaseConnected = false;
      }
    }, 700);
  }

  resetRocket() {
    const status: RocketStatus = {
      system: 'unknown',
      emergency: false,
      flyState: 'unknown',
      isBaseLocked: false
    };

    const orientation: Orientation = {
      x: 0,
      y: 0,
      z: 0,
      w: 0
    };

    this.deviceService.setStatusDown();

    this.rocket.next(
      new Rocket(
        [],
        { isConnected: this.isBaseConnected, isDebugMode: false },
        orientation,
        null,
        null,
        { longitude: 0, latitude: 0 },
        status,
        '',
        new Date()
      )
    );
  }

  serializeFromSerial(data: string) {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.log(e);
      return;
    }

    const isRocket: boolean = data['device_type'] == 'rocket';

    if (!isRocket && data['isRocketConnected'] == false) {
      this.isBaseConnected = true;
      return;
    }

    const rocket = data['rocket'];

    const debugVectors: { x: number; y: number; z: number }[] = [];
    if (isRocket) {
      debugVectors.push(data['debug_vector0']);
      debugVectors.push(data['debug_vector1']);
      debugVectors.push(data['debug_vector2']);
    }

    const orientation: Orientation = rocket['orientation'];

    const coord: Coordinates = {
      latitude: rocket['latitude'] * Math.pow(10, -7),
      longitude: rocket['longitude'] * Math.pow(10, -7)
    };

    const status: RocketStatus = {
      system: rocket['devices']['system']['status'],
      emergency: rocket['emergency'],
      flyState: rocket['flyState'],
      isBaseLocked: isRocket ? false : data['locked']
    };

    Object.keys(rocket['devices']).forEach(el => {
      const device = this.deviceService.getDevice(el as DeviceId);
      if (device !== undefined) {
        this.deviceService.updateDeviceStatus(
          device.id,
          rocket['devices'][el]['status']
        );
      }
    });

    if (!isRocket)
      this.deviceService.updateDeviceStatus('motor', data['motor']['status']);
    this.deviceService.updateBatteryStatus(rocket['devices']['batteryLevel']);

    // Get accelerations
    let acceleration: [Date, Acceleration] = [new Date(), { x: 0, y: 0, z: 0 }];
    if (this.rocket.value !== null) {
      if (
        this.rocket.value.accelerations &&
        this.rocket.value.accelerations.length > 0
      ) {
        acceleration = [new Date(), rocket['acceleration']];
      }
    }

    const accelerations: [Date, Acceleration][] =
      this.rocket.value.accelerations === null
        ? []
        : this.rocket.value.accelerations;
    accelerations.push(acceleration);
    if (accelerations.length > 30) {
      accelerations.splice(0, accelerations.length - 60);
    }

    // Get altitudes
    let altitude: [Date, number] = [new Date(), 0];
    if (this.rocket.value !== null) {
      if (
        this.rocket.value.altitudes &&
        this.rocket.value.altitudes.length > 0
      ) {
        altitude = [new Date(), rocket['altitude']];
      }
    }

    const altitudes: [Date, number][] =
      this.rocket.value.altitudes === null ? [] : this.rocket.value.altitudes;
    altitudes.push(altitude);
    if (altitudes.length > 30) {
      altitudes.splice(0, altitudes.length - 60);
    }

    this.rocket.next(
      new Rocket(
        debugVectors,
        { isDebugMode: isRocket, isConnected: true },
        orientation,
        altitudes,
        accelerations,
        coord,
        status,
        data,
        new Date()
      )
    );
  }
}

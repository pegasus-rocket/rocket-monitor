import { Device, DeviceId, DeviceStatus } from '../models/device.model';
import { Injectable, signal } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DeviceService {
  devices = signal([
    new Device(
      'radio',
      'Radio',
      'unknown',
      'assets/icons/satellite.svg',
      920,
      920
    ),
    new Device(
      'battery',
      'Battery',
      'unknown',
      'assets/icons/battery.svg',
      24,
      24
    ),
    new Device('imu', 'Accel/Gyro', 'unknown', 'assets/icons/mpu.svg', 24, 24),
    new Device(
      'bmp',
      'Barometer',
      'unknown',
      'assets/icons/altitude.svg',
      24,
      24
    ),
    new Device(
      'parachute',
      'Parachute',
      'unknown',
      'assets/icons/parachute.svg',
      24,
      24
    ),
    new Device(
      'motor',
      'Motor',
      'unknown',
      'assets/icons/explosion.svg',
      24,
      24
    )
  ]);

  constructor() {}

  getDevices() {
    return this.devices();
  }

  getDevice(id: DeviceId): Device {
    if (this.devices) {
      return this.devices().find(x => x.id === id);
    }
    return undefined;
  }

  updateDeviceStatus(deviceId: DeviceId, newStatus: DeviceStatus) {
    this.devices.update(device => {
      device.find(x => x.id === deviceId).status = newStatus;
      return device;
    });
  }

  updateBatteryStatus(batteryLevel: number) {
    if (batteryLevel == -1) this.updateDeviceStatus('battery', 'unknown');
    else if (batteryLevel >= 30) this.updateDeviceStatus('battery', 'high');
    else if (batteryLevel < 15) this.updateDeviceStatus('battery', 'critical');
    else if (batteryLevel < 30) this.updateDeviceStatus('battery', 'low');
  }

  setStatusDown() {
    for (let device of this.devices()) {
      device.status = 'unknown';
    }
  }
}

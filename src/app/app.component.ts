import { RocketService } from './services/rocket.service';
import { Component, OnInit } from '@angular/core';
import { listen } from '@tauri-apps/api/event';
import { Rocket } from './models/rocket.model';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: false
})
export class AppComponent implements OnInit {
  rocket = new BehaviorSubject<Rocket>(null);

  constructor(private rocketService: RocketService) {}
  async ngOnInit() {
    this.rocketService.resetRocket();
    await listen<string>('new-serial-message', data => {
      this.rocketService.serializeFromSerial(data.payload);
    });
  }
}

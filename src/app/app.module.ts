import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { MatOptionModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';

import { DeviceStatusSettingsComponent } from './cockpits-view/cockpits/device-status-cockpit/device-status-settings/device-status-settings.component';
import { ThreeDOrientationCockpitsComponent } from './cockpits-view/cockpits/three-d-orientation-cockpits/three-d-orientation-cockpits.component';
import { SerialConfigCockpitComponent } from './cockpits-view/cockpits/serial-config-cockpit/serial-config-cockpit.component';
import { OrientationCockpitComponent } from './cockpits-view/cockpits/orientation-cockpit/orientation-cockpit.component';
import { AltimeterCockpitComponent } from './cockpits-view/cockpits/altimeter-cockpit/altimeter-cockpit.component';
import { RocketActionComponent } from './cockpits-view/cockpits/action-cockpit/rocket-action.component';
import { SpaceOrientationComponent } from './cockpits-view/cockpits/space-orientation/space-orientation.component';
import { DeviceStatusComponent } from './cockpits-view/cockpits/device-status-cockpit/device-status.component';
import { AccelerationComponent } from './cockpits-view/cockpits/acceleration-cockpit/acceleration.component';
import { AltitudeChartComponent } from './cockpits-view/cockpits/altitude-chart/altitude-chart.component';
import { SpeedCockpitComponent } from './cockpits-view/cockpits/speed-cockpit/speed-cockpit.component';
import { SimulationsComponent } from './cockpits-view/cockpits/simulations/simulations.component';
import { MapCockpitComponent } from './cockpits-view/cockpits/map-cockpit/map-cockpit.component';
import { CockpitComponent } from './cockpits-view/cockpits/cockpit/cockpit.component';
import { LogsComponent } from './cockpits-view/cockpits/logs/logs.component';
import { CockpitsComponent } from './cockpits-view/cockpits.component';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { NgOptimizedImage } from '@angular/common';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    CockpitComponent,
    CockpitsComponent,
    SnackbarComponent,
    MapCockpitComponent,
    SimulationsComponent,
    SpeedCockpitComponent,
    DeviceStatusComponent,
    SpeedCockpitComponent,
    RocketActionComponent,
    AccelerationComponent,
    AltitudeChartComponent,
    AltimeterCockpitComponent,
    SpaceOrientationComponent,
    OrientationCockpitComponent,
    SerialConfigCockpitComponent,
    DeviceStatusSettingsComponent,
    ThreeDOrientationCockpitsComponent,
    LogsComponent
  ],
  imports: [
    FormsModule,
    MatCardModule,
    BrowserModule,
    MatInputModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatOptionModule,
    MatSelectModule,
    NgOptimizedImage,
    MatTooltipModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatFormFieldModule,
    RoundProgressModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

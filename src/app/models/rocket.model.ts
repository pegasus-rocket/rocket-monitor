import { DeviceStatus } from './device.model';

export interface Acceleration {
  x: number;
  y: number;
  z: number;
}

export interface Gyro {
  x: number;
  y: number;
  z: number;
  pitch: number;
  roll: number;
}

export interface Orientation {
  x: number;
  y: number;
  z: number;
  w: number;
}

export interface Coordinates {
  longitude: number;
  latitude: number;
}

export type FlyState = 'on_ground' | 'flying' | 'landed' | 'unknown';

export interface RocketStatus {
  system: DeviceStatus;
  emergency: boolean;
  flyState: FlyState;
  isBaseLocked: boolean;
}

interface Serial {
  isConnected: boolean;
  isDebugMode: boolean;
}

export class Rocket {
  // From serial
  public debugVectors: { x: number; y: number; z: number }[];
  public accelerations: [Date, Acceleration][] | null;
  public altitudes: [Date, number][] | null;
  public orientation: Orientation;
  public status: RocketStatus;
  public coord: Coordinates;

  // Out serial
  public serial: Serial;
  public time: Date;
  public rawData: string;

  constructor(
    debugVectors: { x: number; y: number; z: number }[],
    serial: Serial,
    orientation: Orientation,
    altitudes: [Date, number][] | null,
    accelerations: [Date, Acceleration][] | null,
    coord: Coordinates,
    status: RocketStatus,
    rawData: string,
    time: Date
  ) {
    this.debugVectors = debugVectors;
    this.serial = serial;
    this.orientation = orientation;
    this.altitudes = altitudes;
    this.accelerations = accelerations;
    this.coord = coord;
    this.status = status;
    this.rawData = rawData;
    this.time = time;
  }
}

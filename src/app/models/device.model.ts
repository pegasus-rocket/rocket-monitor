export type DeviceId =
  | 'gps'
  | 'launcher'
  | 'radio'
  | 'imu'
  | 'parachute'
  | 'bmp'
  | 'battery'
  | 'motor';

export type DeviceStatus =
  | 'down'
  | 'ready'
  | 'sleep'
  | 'booting'
  | 'connected'
  | 'high'
  | 'low'
  | 'fired'
  | 'critical'
  | 'unknown';

export class Device {
  public id: DeviceId;
  public name: string;
  public status: DeviceStatus;
  public icon: string;
  public imgWidth: number;
  public imgHeight: number;

  constructor(
    id: DeviceId,
    name: string,
    status: DeviceStatus,
    icon: string,
    imgWidth: number,
    imgHeight: number
  ) {
    this.id = id;
    this.name = name;
    this.status = status;
    this.icon = icon;
    this.imgWidth = imgWidth;
    this.imgHeight = imgHeight;
  }
}

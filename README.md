# Rocket Monitor 🚀

<p>
  <a href="https://gitlab.com/esiea-groupe/esiea-app/-/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat-square" alt="ESIEA Assos is released under the GPLv3 license." />
  </a>
</p>

#### A new way to explore the world

![](./doc/img/rocket-monitor-screenshot.png)

Rocket Monitor is built with web technologies😃:

[![Rust](https://img.shields.io/badge/Rust-%23000000.svg?style=for-the-badge&logo=rust&logoColor=white)](https://www.rust-lang.org/)
[![Tauri](https://img.shields.io/badge/Tauri-24C8D8?style=for-the-badge&logo=tauri&logoColor=fff)](https://v2.tauri.app/)
[![Angular](https://img.shields.io/badge/Angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white)](https://angular.dev/)

Rocket monitor is a software to monitor a rocket fly in real time via a radio communication.

## Features ✅

- Rocket's orientation visualisation
- Rocket position (GPS)
- Rocket acceleration and velocity
- Rocket altitude over time
- Module diagnostic (radio, battery, barometer, motor(Launcher), parachute, IMU)
- Parachute ejection
- Motor ignition
- Rocket storage reset
- Received logs are stored
- Denied dangerous behavior
- Serial communication with the base.

## How to contribute 🤝

Thank you for your contribution to the open source world !😍

- opening [issues](https://gitlab.com/pegasus-rocket/rocket-monitor/-/issues)
- Create a MR.

## Development 🏗️

Rocket monitor is written in Rust and Angular (Tauri framework).

### Download Required Tools

Download and install these right away to ensure an optimal Ionic development experience:

- **Node.js** for interacting with the Tauri ecosystem. Download the LTS version [here](https://nodejs.org/en/).
- **A code editor** for... writing code! We are fans of Visual Studio Code.

**Debian/Ubuntu/Linux Mint**

```bash
sudo apt install nodejs npm
```

**Fedora**

```bash
sudo dnf install nodejs npm
```

**Arch Linux**

```bash
sudo pacman -S npm
```

**OpenSUSE**

```bash
sudo zypper install npm
```

To make sure that npm has been installed successfully, run the following command:

```
npm --version
10.8.2
```

### Install the Angular CLI

Install the Ionic CLI with npm:

```shell
npm install -g @angular/cli
```

### Install pnpm

Install pnpm with npm:

```shell
npm install -g pnpm
```

### Get project files

Clone the [repository](https://gitlab.com/pegasus-rocket/rocket-monitor):

```shell
git clone git@gitlab.com:pegasus-rocket/rocket-monitor.git
```

```shell
cd rocket-monitor/
```

### Run the App

Install the dependency:

```shell
pnpm install
```

Run this command next:

```shell
pnpm tauri dev
```

## Licence 📜

The app core is under GPLv3 but be careful the Images are limited are restricted to
copyright.

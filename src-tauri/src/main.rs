// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::io::{self, Read, Write};
use std::sync::{Arc, Mutex};
use std::thread;

use lazy_static::lazy_static;
use serde::Serialize;
use serialport::{SerialPort, SerialPortType};
use tauri::{Emitter, Window};

// Shared resources protected by mutexes
lazy_static! {
    static ref BAUD: Mutex<u32> = Mutex::new(9600);
    static ref PORT: Mutex<String> = Mutex::new(String::new());
    static ref SERIAL: Arc<Mutex<Option<Box<dyn SerialPort>>>> = Arc::new(Mutex::new(None));
}

#[derive(Serialize)]
struct MySerialPortInfo {
    port_name: String,
    port_type: String,
}

/// Updates the serial port settings and initializes the serial connection.
#[tauri::command]
fn update_serial_settings(port: String, baud: u32) -> Result<(), String> {
    {
        let current_port = PORT.lock().unwrap();
        let current_baud = BAUD.lock().unwrap();

        if port == *current_port && baud == *current_baud {
            return Ok(()); // No changes needed
        }
    }

    // Update the shared state
    *BAUD.lock().unwrap() = baud;
    *PORT.lock().unwrap() = port.clone();

    // Reinitialize the serial connection
    init_serial().map_err(|e| format!("Failed to initialize serial connection: {}", e))?;
    println!("Updated serial settings: port={}, baud={}", port, baud);

    Ok(())
}

/// Emits serial messages received from the device to the frontend.
#[tauri::command]
fn read_serial(window: Window) {
    thread::spawn(move || {
        let mut serial_buf: Vec<u8> = vec![0; 1024];
        let mut buffer = String::new();

        loop {
            let result = {
                let mut serial = SERIAL.lock().unwrap();
                if let Some(ref mut port) = *serial {
                    port.read(serial_buf.as_mut_slice())
                } else {
                    eprintln!("Serial port is not initialized.");
                    break;
                }
            };

            match result {
                Ok(bytes_read) => {
                    buffer.push_str(&String::from_utf8_lossy(&serial_buf[..bytes_read]));
                    if buffer.contains('\n') {
                        let lines: Vec<&str> = buffer.split('\n').collect();
                        for line in lines {
                            if !line.trim().is_empty() {
                                println!("{}", line.trim());
                                let _ = window.emit("new-serial-message", line.to_string());
                            }
                        }
                        buffer.clear();
                    }
                }
                Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
                Err(e) => {
                    eprintln!("Serial read error: {}", e);
                    break;
                }
            }
        }
    });
}

/// Retrieves available serial ports.
#[tauri::command]
fn get_serial_ports() -> Result<Vec<MySerialPortInfo>, String> {
    serialport::available_ports()
        .map(|ports| {
            ports
                .into_iter()
                .map(|port| MySerialPortInfo {
                    port_name: port.port_name,
                    port_type: match port.port_type {
                        SerialPortType::UsbPort(info) => format!("USB ({})", info.product.unwrap_or_default()),
                        SerialPortType::PciPort => "PCI".to_string(),
                        SerialPortType::BluetoothPort => "Bluetooth".to_string(),
                        SerialPortType::Unknown => "Unknown".to_string(),
                    },
                })
                .collect()
        })
        .map_err(|e| format!("Failed to list serial ports: {}", e))
}

/// Sends a message to the serial device.
fn write_serial(message: &str) {
    let mut serial = SERIAL.lock().unwrap();
    if let Some(ref mut port) = *serial {
        let data = format!("{}\0", message);
        if let Err(e) = port.write_all(data.as_bytes()) {
            eprintln!("Failed to write to serial port: {}", e);
        }
    } else {
        eprintln!("Serial port is not initialized.");
    }
}

#[tauri::command]
fn release_parachute() {
    write_serial("release_parachute");
}

#[tauri::command]
fn clear_rocket_storage() {
    write_serial("clear_rocket_storage");
}

#[tauri::command]
fn release_rocket() {
    write_serial("release_rocket");
}

/// Initializes the serial port connection.
fn init_serial() -> Result<(), Box<dyn std::error::Error>> {
    let port_name = { PORT.lock().unwrap().clone() };
    let baud_rate = { *BAUD.lock().unwrap() };

    let new_port = serialport::new(&port_name, baud_rate).open()?;

    let mut serial = SERIAL.lock().unwrap();
    *serial = Some(new_port);

    println!("Initialized serial port: {}, baud: {}", port_name, baud_rate);
    Ok(())
}

fn main() {
    tauri::Builder::default()
        .plugin(tauri_plugin_global_shortcut::Builder::new().build())
        .plugin(tauri_plugin_shell::init())
        .plugin(tauri_plugin_clipboard_manager::init())
        .plugin(tauri_plugin_process::init())
        .plugin(tauri_plugin_dialog::init())
        .plugin(tauri_plugin_http::init())
        .plugin(tauri_plugin_notification::init())
        .plugin(tauri_plugin_os::init())
        .plugin(tauri_plugin_fs::init())
        .invoke_handler(tauri::generate_handler![
            get_serial_ports,
            update_serial_settings,
            read_serial,
            release_parachute,
            release_rocket,
            clear_rocket_storage
        ])
        .run(tauri::generate_context!())
        .expect("Error while running Tauri application");
}
